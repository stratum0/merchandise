Banner
======

Notfalls kann auch die [Fahne](../Fahne/) als Banner verwendet werden.

banner-sanduhr-pixel-quer-vga-250x100
-------------------------------------

![Vorschau](banner-sanduhr-pixel-quer-vga-250x100.svg)

* Entwurf von rohieb.
* Sanduhr im Retro-Look, nicht als "offizielle" Version mit CI gedacht, sondern
  für Community-Events.
* Die SVG-Version ist im Maßstab 1:1, die PDF-Version ist auf 2,50 m×1,00 m für
  direkten Flyeralarm-Upload hochskaliert und mit Rand nach Datenblatt versehen.
* [Flyeralarm-Produktlink](https://www.flyeralarm.com/de/shop/configurator/index/id/141/planen-rechteck.html#633=2381&634=2388&635=2429&636=2380) (mit Ösen und Saum an allen vier Seiten)
* Lizenz:

    > Der Copyrightinhaber, @rohieb, erlaubt freie Verwendung für alles
    > Stratum-0-bezogene.

<!-- vim: set ft=markdown et ts=4 sw=4 : -->
