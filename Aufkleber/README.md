Aufkleber
=========

aufkleber-rund-schwarz-flyeralarm-50mm
--------------------------------------

![Vorschau](aufkleber-rund-schwarz-flyeralarm-50mm.png)

* Entwurf von rohieb.
* Runde Aufkleber mit weißem Logo auf schwarzem Hintergrund
* [Flyeralarm-Produktlink](https://www.flyeralarm.com/de/shop/configurator/index/id/34/outdoor-aufkleber.html#159=601&160=6204&161=615&162=585)
* Lizenz:

    > Der Copyrightinhaber, @blinry, erlaubt freie Verwendung des
    > Stratum-0-Logos für alles und jeden. Die einzige Bedingung ist, dass man
    > ihn auf Nachfrage als Urheber nennt.

    > Den Rest drumrum hat @rohieb zusammengeschoben und findet, das darf für
    > alles Stratum-0-bezogene ohne Nachfrage benutzt werden.

Das Inkscape-SVG enthält mehrere Layer, darunter auch den Sicherheitsabstand und
Beschnittzugabe nach dem Flyeralarm-Datenblatt. Das Makefile erzeugt aus dem
Layer "Druckbereich" dann ein Flyeralarm-kompatibles PDF und ein Vorschau-PNG.


aufkleber-rund-weiß-ringtext-flyeralarm-50mm
--------------------------------------------

![Vorschau](aufkleber-rund-weiß-ringtext-flyeralarm-50mm.png)

* Entwurf von rohieb.
* Runde Aufkleber mit schwarzem Logo auf weißem Hintergrund, mit Ringtext
  "Hackerspace Braunschweig" und URL
* [Flyeralarm-Produktlink](https://www.flyeralarm.com/de/shop/configurator/index/id/34/outdoor-aufkleber.html#159=601&160=6204&161=615&162=585)
* Lizenz:

    > Der Copyrightinhaber, @blinry, erlaubt freie Verwendung des
    > Stratum-0-Logos für alles und jeden. Die einzige Bedingung ist, dass man
    > ihn auf Nachfrage als Urheber nennt.

    > Den Rest drumrum hat @rohieb zusammengeschoben und findet, das darf für
    > alles Stratum-0-bezogene ohne Nachfrage benutzt werden.

Das Inkscape-SVG enthält mehrere Layer, darunter auch den Sicherheitsabstand und
Beschnittzugabe nach dem Flyeralarm-Datenblatt. Das Makefile erzeugt aus dem
Layer "Druckbereich" dann ein Flyeralarm-kompatibles PDF und ein Vorschau-PNG.


aufkleber-sanduhr-pixel-flyeralarm-dina8
----------------------------------------

![Vorschau](aufkleber-sanduhr-pixel-flyeralarm-dina8.png)

* Entwurf von rohieb.
* Sanduhr im Retro-Look, mal was anderes als immer nur das alte schwarz-weiß :)
* Design für [Flyeralarm DIN A8
  Outdoor-Aufkleber](https://www.flyeralarm.com/de/shop/configurator/index/id/34/outdoor-aufkleber.html#159=582&160=583),
  kann natürlich beliebig skaliert werden.
* Lizenz:

    > Der Copyrightinhaber, @rohieb, erlaubt freie Verwendung für alles
    > Stratum-0-bezogene.

<!-- vim: set ft=markdown et ts=4 sw=4 : -->
