Design-Vorlagen für Stratum 0-Merchandise
=========================================

Pro Entwurf sollte folgendes vorhanden sein:

1.  Die Quelldatei(en), bevorzugt als ein mit Open-Source-Programmen lesbares
    Format, z.B. Inkscape-SVG.
    Falls es mehrere Quelldateien erforderlich sind, bietet sich an, einen extra
    Unterordner für das Design anzulegen.
2.  Ein fertig gerendertes Zielformat (PDFs/EPS/…) zum direkten Hochladen zum
    Druckdienstleister
4.  Ein Eintrag in der README.md des entsprechenden Unterordners mit mehr
    Informationen (Link auf bestelltes Druckprodukt, Lizenz, etc.)
3.  Optional auch noch ein Vorschaubild als PNG

In manchen Ordnern gibt es Makefiles, die die PDFs und PNGs automatisch
erstellen. Diese Infrastruktur darf ausdrücklich genutzt werden und @rohieb
erklärt auch gerne brav, wie das alles funktioniert :)

__siehe auch: <https://gitli.stratum0.org/stratum0/logos/>__

<!-- vim: set ft=markdown et ts=4 sw=4 : -->
