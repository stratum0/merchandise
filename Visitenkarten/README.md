Visitenkarten
-------------

![Vorschaubild](visitenkarte-beidseitig-flyeralarm-90x50mm.png)

* Entwurf von rohieb
* Stratum-0-Visitenkarten mit weißer Rückseite für Notizen
* Die Rückseite und die Vorderseite sind in eigenen Dateien nach dem
  Flyeralarm-Datenblatt angelegt und haben mehrere Layer mit Druckdaten,
  Schnittzugabe etc.
  Aus dem Druckdaten-Layer erzeugt das Makefile die PDFs, und
  `visitenkarte-beidseitig-flyeralarm-90x50mm.pdf` kann direkt als Upload für
  [Flyeralarm-Visitenkarten,
  9×5 cm](https://www.flyeralarm.com/de/shop/configurator/index/id/5625/visitenkarten-klassiker.html#1286=15404&1287=5796)
  verwendet werden.
  (Sollte klappen – wenn erfolgreich getestet, entferne bitte diesen Satz.)

Lizenz:

> Dieses Werk wurde unter der Lizenz [CC BY-SA 3.0][ccbysa30] veröffentlicht,
> das bedeutet:
>
> Dieses Werk darf von dir
>
> *   verbreitet werden – vervielfältigt, verbreitet und öffentlich zugänglich
>     gemacht werden
> *   neu zusammengestellt werden – abgewandelt und bearbeitet werden
>
> Zu den folgenden Bedingungen:
>
> *   Namensnennung – Du musst den Namen des Autors/Rechteinhabers (rohieb) in
>     der von ihm festgelegten Weise nennen (aber nicht so, dass es so aussieht,
>     als würde er dich oder deine Verwendung des Werks unterstützen).
> *   Weitergabe unter gleichen Bedingungen – Wenn du das lizenzierte Werk bzw.
>     den lizenzierten Inhalt bearbeitest, abwandelst oder in anderer Weise
>     erkennbar als Grundlage für eigenes Schaffen verwendest, darfst du die
>     daraufhin neu entstandenen Werke bzw. Inhalte nur unter Verwendung von
>     Lizenzbedingungen weitergeben, die mit denen dieses Lizenzvertrages
>     identisch, vergleichbar oder kompatibel sind.
>
> Der Copyrightinhaber, rohieb, erlaubt außerdem die freie Verwendung für
> alles Stratum-0-bezogene.

[ccbysa30]: http://creativecommons.org/licenses/by-sa/3.0/deed.de
