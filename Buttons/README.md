Buttons
=======

Vorlagen für die [Buttonmaschine](https://stratum0.org/wiki/Buttonmaschine) im Space.

stratum0-logos-din-a4-ibp-25mm
------------------------------

* Entwurf von @rohieb
* 15 weiße und 20 schwarze Button-Einleger mit Stratum 0-Logo
* für die Rohlinge von [IBP-Schollenberger](http://www.ibp-schollenberger.de/)
  mit 25 mm Durchmesser
* Lizenz:

    > Der Copyrightinhaber, @rohieb, erlaubt freie Verwendung für alles
    > Stratum-0-bezogene.

<!-- vim: set ft=markdown et ts=4 sw=4 : -->
